import { Component, ViewChild, Renderer2 } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/merge';

/**
 * Generated class for the CanvasDrawComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'canvas-draw',
  templateUrl: 'canvas-draw.html'
})

export class CanvasDrawComponent {

  @ViewChild('canvasDraw') canvas: any;
  canvasElement: any;
  lastX: number;
  lastY: number;
  mouseClicked: boolean;
  currentColor: string;
  currentSize: number;
  availableColors: any;

  constructor(public platform: Platform, public renderer: Renderer2) {
    this.mouseClicked = false;
    this.currentSize = 5;
    this.currentColor = '#6173f4';
    this.availableColors = [
      '#6173f4', '#f64662', '#2185d5', '#00b8a9',
      '#ff6600', '#5585b5', '#a03232', '#65d269'
    ];
  }

  ngAfterViewInit() {
    this.canvasElement = this.canvas;

    this.renderer.setAttribute(
      this.canvasElement.nativeElement, 'width', this.platform.width() + ''
    );
    this.renderer.setAttribute(
      this.canvasElement.nativeElement, 'height', this.platform.height() + ''
    );

    Observable.fromEvent(this.canvasElement.nativeElement, 'mousedown')
      .subscribe((event) => {
        this.handleTouchStart(event);
      });

    Observable.fromEvent(this.canvasElement.nativeElement, 'mousemove')
      .subscribe((event) => {
        this.handleTouchMove(event);
      });

    Observable.fromEvent(this.canvasElement.nativeElement, 'mouseup')
      .subscribe(() => {
        this.handleTouchEnd();
      });
  }

  handleTouchStart(event) {
    this.mouseClicked = true;
    this.lastX = event.pageX;
    this.lastY = event.pageY;
  }

  handleTouchMove(event) {
    if (this.mouseClicked) {

      let ctx = this.canvasElement.nativeElement.getContext('2d');
      ctx.beginPath();
      ctx.lineJoin = 'round';
      ctx.moveTo(this.lastX, this.lastY);
      ctx.lineTo(event.pageX, event.pageY);
      ctx.strokeStyle = this.currentColor;
      ctx.lineWidth = this.currentSize;
      ctx.closePath();
      ctx.stroke();

      this.lastX = event.pageX;
      this.lastY = event.pageY;
    }
  }

  handleTouchEnd() {
    this.mouseClicked = false;
  }

  changeColor(color) {
    this.currentColor = color;
    this.mouseClicked = false;
  }

  changeSize(size) {
    this.currentSize = size;
    this.mouseClicked = false;
  }

}
